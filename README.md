# Proyecto Springboot

* Define la última versión disponible de springboot como base (a noviembre 2021 es la 2.5.4.RELEASE)
* Sugiere la utilización de Junit para tests unitarios
* Sugiere la utilización de la especificación Swagger, mediante la implementación de Springfox, para exponer la documentación de la [API](http://localhost:8080/swagger-ui). 
* El proyecto trabaja con una base de datos local h2. Acceder a la  [consola H2](http://localhost:8080/h2-console) ver el archivo **application.yml** para extraer los datos de conexión

* El parser xml se realiza con JABAX


Para ejecutar el proyecto en forma local basta con ejecutar el siguiente comando:

```bash
mvn spring-boot:run 


El proyecto cuenta con la posibilidad de ser levantado en un entorno containerizado.

Como requisito previo se requiere la instalación de [docker](https://docs.docker.com/install/).

Luego se deberán ejecutar los siguientes comandos:

```bash
mvn clean install

docker build -t nombre_imagen .

docker run -d -p 8080:8080 nombre_imagen
```


* [Documentación de la API](http://localhost:8080/swagger-ui/)
* [Springboot Docs](https://spring.io/projects/spring-boot)
* [Documentación Docker](https://docs.docker.com/)

