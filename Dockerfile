#
# Build stage
#
FROM maven:3.6.0-jdk-11-slim AS build
WORKDIR /app
COPY src ./src
COPY src/main/resources/data-example.xml .
COPY pom.xml .
RUN mvn -f pom.xml clean package

#
# Package stage
#

FROM openjdk:11
WORKDIR /app
COPY --from=build /app/target/micro.jar .
COPY --from=build /app/data-example.xml .
EXPOSE 8080
ENTRYPOINT ["java","-jar","micro.jar"]















