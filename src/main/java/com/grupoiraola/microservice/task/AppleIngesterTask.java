package com.grupoiraola.microservice.task;

import com.grupoiraola.microservice.domain.Catalog;
import com.grupoiraola.microservice.service.ModelServiceInterface;
import com.grupoiraola.microservice.util.ParserUtil;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;



@Slf4j
@Service
public class AppleIngesterTask {

	@Value("${files.file-xml}")
	private Resource res;	
	
	
	@Autowired
	private ParserUtil parserUtil;
	
	@Autowired
	private ModelServiceInterface modelService;

	@Scheduled(cron = "${app.data.ingester.runCron}")
	public void ingestFile() {
		log.info("Inicia el cron.");
		Catalog catalog;
		try {
			catalog = parserUtil.unMarshalingCatalog();
			catalog.getModels()
				.stream()
				.forEach(each -> 
					modelService.findByNameAndModelIsNull(each.getName())
					.ifPresentOrElse(model -> log.error("Mobile duplicado: " + model) ,()-> modelService.save(each) )
				);
			log.info("Fin del cron.");
		} catch (Exception e) {
			log.error("Cron Error:", e);
		} 
 
	}
}
