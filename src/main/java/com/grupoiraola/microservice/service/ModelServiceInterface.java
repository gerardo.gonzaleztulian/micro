package com.grupoiraola.microservice.service;

import java.util.List;
import java.util.Optional;

import com.grupoiraola.microservice.domain.Especificacion;
import com.grupoiraola.microservice.domain.Model;

public interface ModelServiceInterface {
	
	Model save(Model model);
	List<Model> findAll();
	Optional<Model> findById(Long id);
	Optional<Model> findByIdAndModelIsNull(Long id);
	Optional<Model> findByNameAndModelIsNull(String name);
	List<Especificacion> findEspecificacionByMarca(String name);
	public Model updateModelValues(Model model, Model modelValues);
}
