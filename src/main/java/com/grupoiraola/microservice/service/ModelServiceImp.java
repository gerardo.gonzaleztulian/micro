package com.grupoiraola.microservice.service;

import com.grupoiraola.microservice.domain.Especificacion;
import com.grupoiraola.microservice.domain.Model;
import com.grupoiraola.microservice.repository.ModelEntityInterface;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;



import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class ModelServiceImp implements ModelServiceInterface {

	@Autowired
	private ModelEntityInterface modelRepository;
	
	@Override
	public Model save(Model model) {
		log.info("Persistiendo model: " + model);
		return modelRepository.save(model);
	}
	
	@Override
	public List<Model> findAll() {
		log.info("findAll().");
		return modelRepository.findAll();
	}
		
	@Override
	public Optional<Model> findById(Long id) {
		log.info("findById: " + id.toString());
		return modelRepository.findById(id);
	}
		
	public Optional<Model> findByIdAndModelIsNull(Long id) {
		log.info("findByIdAndModelIsNull: " + id);
		return modelRepository.findByIdAndModelIsNull(id);
	}
	
	public Optional<Model> findByNameAndModelIsNull(String name) {
		log.info("findByIdAndModelIsNull: " + name);
		return modelRepository.findByNameAndModelIsNull(name);
	}
	
	public List<Especificacion> findEspecificacionByMarca(String name) {
		log.info("findEspecificacionByMarca().");
		return modelRepository.findModelByMarca(name).stream().map(each -> new Especificacion(each)).collect(Collectors.toList());
				
	}

	public Model updateModelValues(Model model, Model modelValues) {
		model.setName(modelValues.getName());
		model.setYear(modelValues.getYear());
		model.getCamera().setSize(modelValues.getCamera().getSize());
		model.getCamera().setType(modelValues.getCamera().getType());
		model.getScreen().setSize(modelValues.getScreen().getSize());
		model.getScreen().setType(modelValues.getScreen().getType());
		model.setCapacities(modelValues.getCapacities());
		model.setColors(modelValues.getColors());
		return model;
		
	}
	
}
