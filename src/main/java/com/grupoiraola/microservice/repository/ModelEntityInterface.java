package com.grupoiraola.microservice.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.grupoiraola.microservice.domain.Model;




public interface ModelEntityInterface extends JpaRepository<Model, Long> {
	Optional<Model> findById( Long id );
	Optional<Model> findByIdAndModelIsNull(Long id );
	Optional<Model> findByNameAndModelIsNull(String name );

	@Query("select child from Model child where exists " 
		+ "(select 1 from Model root where child.model = root and root.name Like %:name%) "
		+ "or (child.name Like %:name% and child.model is null)")
	List<Model> findModelByMarca(@Param("name") String name);
	

	
}




