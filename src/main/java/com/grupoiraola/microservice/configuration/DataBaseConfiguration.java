package com.grupoiraola.microservice.configuration;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@Configuration
@EntityScan(basePackages = {"com.grupoiraola.microservice"})
@EnableJpaRepositories(basePackages = {"com.grupoiraola.microservice"})
public class DataBaseConfiguration {
}
