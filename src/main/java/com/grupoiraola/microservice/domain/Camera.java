package com.grupoiraola.microservice.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MapsId;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import org.eclipse.persistence.oxm.annotations.XmlInverseReference;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="CAMERA")
@XmlRootElement(name="camera")
@XmlAccessorType(XmlAccessType.FIELD)
public class Camera {

	@JsonIgnore
	@XmlTransient
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;	
	
	
    @XmlAttribute
    @Column(name = "SIZE")
	String size;
    
    @XmlAttribute
    @Column(name = "TYPE")
	String type;
    
    @JsonBackReference
    @XmlTransient
    @XmlInverseReference(mappedBy = "camera")
    @OneToOne(fetch = FetchType.LAZY)
    @MapsId
    private Model model;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getSize() {
		return size;
	}

	public void setSize(String size) {
		this.size = size;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Model getModel() {
		return model;
	}

	public void setModel(Model model) {
		this.model = model;
	}

    @Override
    public String toString() {
        return String.format("Camera: { id: " + id + " size: " + size + " type: " + type +  "}" );
    }	
	
    public void afterUnmarshal(final Unmarshaller u, final Object parent) {
        this.setModel((Model)parent);
    }
}
