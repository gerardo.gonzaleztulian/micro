package com.grupoiraola.microservice.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlValue;
import org.eclipse.persistence.oxm.annotations.XmlInverseReference;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;


@Entity
@Table(name="COLOR")
@XmlRootElement(name = "color")
@XmlAccessorType(XmlAccessType.FIELD)
public class Color {
	@JsonIgnore
	@XmlTransient
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    
	@XmlValue
    private String color;
    
	@JsonBackReference
	@XmlTransient
    @XmlInverseReference(mappedBy = "colores")
    @ManyToOne
    @JoinColumn(name="model_id", referencedColumnName = "id")
    private Model model;

    @XmlAttribute
    @Transient 
    private String dummy;	
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public Model getModel() {
		return model;
	}

	public void setModel(Model model) {
		this.model = model;
	}
	
    @Override
    public String toString() {
        return String.format("Color { color : " +  color +  " }" );
    }	
	
    public void afterUnmarshal(final Unmarshaller u, final Object parent) {
        this.setModel((Model)parent);
    }
    
 
}
