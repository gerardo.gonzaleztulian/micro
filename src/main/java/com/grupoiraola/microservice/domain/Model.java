package com.grupoiraola.microservice.domain;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import org.eclipse.persistence.oxm.annotations.XmlInverseReference;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlAccessType;


@Entity
@Table(name="MODEL")
@XmlRootElement(name="model")
@XmlAccessorType(XmlAccessType.FIELD)
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class Model {

	@XmlTransient
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
	
    @Column(name = "NAME")
	@XmlAttribute
    String name;

    @Column(name = "YEAR")
	@XmlAttribute
    int year;
    
    @JsonManagedReference
    @XmlElement(name = "screen")
    @OneToOne(mappedBy = "model", cascade = CascadeType.ALL, fetch = FetchType.LAZY, optional = false)
    Screen screen;
    
    @JsonManagedReference
    @XmlElement(name = "camera")
    @OneToOne(mappedBy = "model", cascade = CascadeType.ALL, fetch = FetchType.LAZY, optional = false)
    Camera camera;
    
    @JsonManagedReference
    @XmlElementWrapper(name = "capacities")
    @XmlElement(name = "capacity")
    @OneToMany(mappedBy = "model", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    List<Capacity> capacities = new ArrayList<Capacity>(); ;

    @JsonManagedReference
    @XmlElementWrapper(name = "colors")
    @XmlElement(name = "color")
    @OneToMany(mappedBy = "model", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    List<Color> colors = new ArrayList<Color>();    
    
    @JsonProperty("submodels")
    @JsonManagedReference   
	@XmlElementWrapper(name = "submodels")
    @XmlElement(name = "model" , required = false )
    @OneToMany(mappedBy="model", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private List<Model> models;
    
    @JsonBackReference
    @XmlTransient
    @ManyToOne()
    @XmlInverseReference(mappedBy = "models")
    @JoinColumn(name="model_id", referencedColumnName = "id")
    private Model model;  
    
    
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}


	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}


	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

  
	public Screen getScreen() {
		return screen;
	}

	public void setScreen(Screen screen) {
		this.screen = screen;
	}
    
	
    public Camera getCamera() {
		return camera;
	}

	public void setCamera(Camera camera) {
		this.camera = camera;
	}

	public List<Capacity> getCapacities() {
		return capacities;
	}

	public void setCapacities(List<Capacity> capacities) {
		this.capacities = capacities;
	}

	public List<Color> getColors() {
		return colors;
	}

	public void setColors(List<Color> colors) {
		this.colors = colors;
	}


	
    public List<Model> getModels() {
		return models;
	}

	public void setModels(List<Model> models) {
		this.models = models;
	}

	public Model getModel() {
		return model;
	}

	public void setModel(Model model) {
		this.model = model;
	}

    @Override
    public String toString() {
        return String.format("Model: { id: " + id + " name: " + name + " year: " + year +  " screen: " +  screen  + " camera: " + camera +  " } " );
    }
    
    public void afterUnmarshal(final Unmarshaller u, final Object parent) {
        this.setModel((Model)parent);
    }   
    
}
