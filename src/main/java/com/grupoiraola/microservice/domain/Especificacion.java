package com.grupoiraola.microservice.domain;

import java.util.List;

public class Especificacion {
	private Long id;
	private String name;
	private int year;
	private Screen screen;
	private Camera camera;
	private List<Capacity> capacities;
	private List<Color> colors;
	
	public Especificacion (Model model) {
		this.id = model.getId();
		this.name = model.getName();
		this.year = model.getYear();
		this.screen = model.getScreen();
		this.camera = model.getCamera();
		this.capacities = model.getCapacities();
		this.colors = model.getColors();
	}
	
	
	public Long getId() {
		return id;
	}




	public void setId(Long id) {
		this.id = id;
	}




	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getYear() {
		return year;
	}
	public void setYear(int year) {
		this.year = year;
	}
	public Screen getScreen() {
		return screen;
	}
	public void setScreen(Screen screen) {
		this.screen = screen;
	}
	public Camera getCamera() {
		return camera;
	}
	public void setCamera(Camera camera) {
		this.camera = camera;
	}

	public List<Capacity> getCapacities() {
		return capacities;
	}


	public void setCapacities(List<Capacity> capacities) {
		this.capacities = capacities;
	}


	public List<Color> getColors() {
		return colors;
	}
	public void setColors(List<Color> colors) {
		this.colors = colors;
	}
	
    @Override
    public String toString() {
        return String.format("Especificacion: {  name: " + name + " year: " + year +  " screen: " +  screen  + " camera: " + camera +  " } " );
    }
	
}
