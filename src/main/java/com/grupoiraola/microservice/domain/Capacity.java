package com.grupoiraola.microservice.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlValue;
import org.eclipse.persistence.oxm.annotations.XmlInverseReference;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;


@Entity
@Table(name="CAPACITY")
@XmlRootElement(name = "capacity")
@XmlAccessorType(XmlAccessType.FIELD)
public class Capacity {
	
	@JsonIgnore
	@XmlTransient
	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    
    @XmlValue
    private  String capacity;
    
    @JsonBackReference
    @XmlTransient 
	@XmlInverseReference(mappedBy = "capacities")
    @ManyToOne
    @JoinColumn(name="model_id", referencedColumnName = "id")
    private Model model;

    @XmlAttribute
    @Transient 
    private String dummy;    
    
    
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	public String getCapacity() {
		return capacity;
	}

	public void setCapacity(String capacity) {
		this.capacity = capacity;
	}

	public Model getModel() {
		return model;
	}

	public void setModel(Model model) {
		this.model = model;
	}
	
    @Override
    public String toString() {
        return String.format("Capacity: { capacity : " + capacity  +  " }" );
    }	
	
    public void afterUnmarshal(final Unmarshaller u, final Object parent) {
        this.setModel((Model)parent);
    }
    

}
