package com.grupoiraola.microservice.controller;

import com.grupoiraola.microservice.domain.Especificacion;
import com.grupoiraola.microservice.domain.Model;
import com.grupoiraola.microservice.exception.ModelNotFoundException;
import com.grupoiraola.microservice.service.ModelServiceInterface;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import lombok.extern.slf4j.Slf4j;

@RestController
@Slf4j
@RequestMapping("v1/mobile")
public class MobileController {

	@Autowired
	private ModelServiceInterface modelService;	
	
	@GetMapping
	public List<Model> findAll() {
		log.info("Request findAll.");
		return modelService.findAll();
	}
	
	@GetMapping(value ="/{id}")
	public Model findByIdAndModelIsNull(@PathVariable("id") Long id) {
		log.info("Request findByIdAndModelIsNull: " + id.toString());
		return modelService
				.findByIdAndModelIsNull(id)
				.orElseThrow(() -> new ModelNotFoundException(id));		
	}
	
	@GetMapping(value = "/especificacion")
	public List<Especificacion> FindByMarca(@RequestParam("marca") String name){
		log.info("Request FindByMarca: " + name);
		return modelService.findEspecificacionByMarca(name);
	}
	
	@PutMapping 
	public Model save(@RequestBody Model model) {
		log.info("Request save: " + model);
		return 	modelService
				.findById(model.getId())
				.map(m ->   modelService.save(
						modelService.updateModelValues(m, model))	
				)
				.orElseThrow(() -> new ModelNotFoundException(model.getId())
			);
					
	}
}
