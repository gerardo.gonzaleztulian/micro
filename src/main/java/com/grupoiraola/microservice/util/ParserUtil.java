package com.grupoiraola.microservice.util;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.StringWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import java.text.SimpleDateFormat;
import java.util.ArrayList;

import java.util.Date;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;
import org.w3c.dom.Document;

import lombok.extern.slf4j.Slf4j;

import com.grupoiraola.microservice.domain.Catalog;
import com.grupoiraola.microservice.domain.Model;


@Component
@Slf4j
public class ParserUtil {

	@Value("${files.file-xml}")
	private Resource res;
	
	public String marshalingCatalog(Catalog catalog) throws JAXBException
	{
		ByteArrayOutputStream os = new ByteArrayOutputStream();
	    JAXBContext jaxbContext = JAXBContext.newInstance(Catalog.class);
	    Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
	 
	    jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
	    jaxbMarshaller.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");
	    jaxbMarshaller.setProperty("com.sun.xml.bind.xmlDeclaration", false);
	    jaxbMarshaller.setProperty("com.sun.xml.bind.xmlHeaders", "<?xml version=\'1.0\' encoding=\'UTF-8\'?>"); 
	    jaxbMarshaller.marshal( catalog , os);
	    return new String(os.toByteArray()).replace("\n", System.getProperty("line.separator"));
	}
	
	public Catalog unMarshalingCatalog() throws JAXBException, IOException, XMLStreamException {
		Catalog catalog = new Catalog();
		
		if (res.exists() && res.getFile().exists()) {
			JAXBContext jc = JAXBContext.newInstance(Model.class);
			XMLInputFactory xif = XMLInputFactory.newFactory();
			StreamSource xml = new StreamSource(res.getInputStream());
			XMLStreamReader xsr = xif.createXMLStreamReader(xml);
			List<Model> models = new ArrayList<Model>();
			Unmarshaller unmarshaller = jc.createUnmarshaller();
			while(xsr.getEventType() != XMLStreamReader.END_DOCUMENT) {
				if(xsr.isStartElement() && "model".equals(xsr.getLocalName())) {
					Model model = (Model) unmarshaller.unmarshal(xsr);
					models.add(model);
				}
				xsr.next();
			}
			catalog.setModels(models);
			//renameFile();
		} else 
		{
			log.debug("No se encontro archivo.");
		}
		return catalog;
	}
	
	private void renameFile( ) throws IOException {		
		SimpleDateFormat formateador = new SimpleDateFormat("yyyymmddhhmmss");
		Path source = Paths.get(res.getFile().getAbsolutePath());
		Files.move(source, source.resolveSibling(formateador.format(new Date()).concat(".xml")));
	}
	
	
	public  Document convertXMLFileToXMLDocument(String filePath) 
	{
	    //Parser that produces DOM object trees from XML content
	    DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
	     
	    //API to obtain DOM Document instance
	    DocumentBuilder builder = null;
	    try
	    {
	        //Create DocumentBuilder with default configuration
	        builder = factory.newDocumentBuilder();
	         
	        //Parse the content to Document object
	        Document xmlDocument = builder.parse(new File(filePath));
	         
	        return xmlDocument;
	    } 
	    catch (Exception e) 
	    {
	        e.printStackTrace();
	    }
	    return null;
	}
	
	public String writeXmlDocumentToXmlFile(Document xmlDocument){
	    TransformerFactory tf = TransformerFactory.newInstance();
	    Transformer transformer;
	    try {
	        transformer = tf.newTransformer();
	         
	        // Uncomment if you do not require XML declaration
	        // transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
	         
	        //A character stream that collects its output in a string buffer, 
	        //which can then be used to construct a string.
	        StringWriter writer = new StringWriter();
	 
	        //transform document to string 
	        transformer.transform(new DOMSource(xmlDocument), new StreamResult(writer));
	    
	        return   writer.getBuffer().toString();  
	    }
	    catch (TransformerException e) 
	    {
	        e.printStackTrace();
	    }
	    catch (Exception e) 
	    {
	        e.printStackTrace();
	    }
	    return null;
	}
	
}
