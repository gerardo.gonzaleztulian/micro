package com.grupoiraola.microservice;

import static org.junit.Assert.assertEquals;


import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import com.grupoiraola.microservice.domain.Catalog;
import com.grupoiraola.microservice.service.ModelServiceImp;
import com.grupoiraola.microservice.util.ParserUtil;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@SpringBootTest

@TestInstance(Lifecycle.PER_CLASS)
public class ModelServiceTest {

	
	@Autowired
	ModelServiceImp modelService;
	
	@Autowired
	private ParserUtil parserUtils;
	
	
	@BeforeAll
	
    public void setup() {
        log.info("setup");

		Catalog catalog;
		try {
			catalog = parserUtils.unMarshalingCatalog();
			catalog.getModels()
				.stream()
				.forEach(each -> 
					modelService.findByNameAndModelIsNull(each.getName())
					.ifPresentOrElse(model -> log.error("Mobile duplicado: " + model) ,()-> modelService.save(each) )
				);
			log.info("Informacion persistida.");
		} catch (Exception e) {
			log.error("Errortest:", e);
		} 
    }
	
	
    @DisplayName("Model Service getName")
    @Test
    void testModelGetName() {
    	assertEquals("iPhone 12", modelService.findById(1L).get().getName());
    }
	
    @DisplayName("Model Service getYear")
    @Test
    void testModelGetYear() {
    	assertEquals(2020, modelService.findById(1L).get().getYear());
    }
    
    @DisplayName("Model Service getCountColors")
    @Transactional
    @Test
    void testModelGetCountColors() {
	    	assertEquals(6,modelService.findById(1L).get().getColors().size());
    }

    @DisplayName("Model Service getCountCapacities")
    @Transactional
    @Test
    void testModelGetCountCapacities() {
    	assertEquals(3, modelService.findById(1L).get().getCapacities().stream().count());
    }    
    
    @DisplayName("Model Service getCountSubModels")
    @Transactional
    @Test
    void testModelGetCountSubModles() {
    	assertEquals(3, modelService.findById(1L).get().getModels().stream().count());
    }
    
    @DisplayName("Model Service getCountSubModels")
    @Transactional
    @Test
    void testModelGetNameFirstSubModles() {
    	assertEquals("iPhone 12 Pro", modelService.findById(1L).get().getModels().stream().findFirst().get().getName());
    }
    
    @DisplayName("Model Service getCountEspcificacionByMarca")
    @Test
    void testEspecificacionGetCountEspcificacionByMarca() {
    	assertEquals(3, modelService.findEspecificacionByMarca("iPhone 11").stream().count());
    }
    
    @DisplayName("Model Service ModelfindByIdAndModelIsNullNotFound()")
    @Test
    void testModelfindByIdAndModelIsNullNotFound() {
    	assertEquals(false, modelService.findByIdAndModelIsNull(9999999L).isPresent() );
    }
}
