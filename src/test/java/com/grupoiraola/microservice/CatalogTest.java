package com.grupoiraola.microservice;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.io.IOException;
import java.nio.charset.StandardCharsets;


import javax.xml.bind.JAXBException;
import javax.xml.stream.XMLStreamException;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.Resource;

import com.grupoiraola.microservice.domain.Catalog;
import com.grupoiraola.microservice.util.ParserUtil;



@SpringBootTest
public class CatalogTest {

	@Autowired
	private ParserUtil parserUtils;
	
	@Value("${files.file-xml}")
	private Resource res;
	
	
	@Test 
	void fileToEntities() throws JAXBException, IOException, XMLStreamException {
		Catalog catalog = parserUtils.unMarshalingCatalog();
		//String xml = new String(res.getInputStream().readAllBytes(), StandardCharsets.UTF_8);
		String jbax = parserUtils.marshalingCatalog(catalog);
		assertNotNull(jbax);
	}
	
}
